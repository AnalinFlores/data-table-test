let cargaPagina = function(){
	listaTiendas();
};


function listaTiendas(){
    let url= "./../data/assignedStore.json";
    
    $.getJSON(url, function(assignedStore){
        muestraTiendas(assignedStore.data);     
    });  
}

function muestraBranData(listData){
    listData.forEach(function(data){
        return data;
    });
}

function muestraTiendas(listaTiendas){
 listaTiendas.forEach(function(tienda){
    let newString = `<tr class="product">
    <td class="name">${tienda.name}</td>
    <td class="paseante">${""}</td>
    <td class="visitas">${""}</td>
    <td class="atraccion">${""}</td>
    <td class="gabinete">${""}</td>
    <td class="tickets">${""}</td>
    <td class="persuasion">${""}</td>
    <td class="ingresos">${""}</td>
    <td class="ingresoTicket">${""}</td>
    <td class="articulo">${""}</td>
    <td class="articuloItem">${""}</td>  
    <td class="averagePermanence">${""}</td>   
    </tr>`; 

    let newTr = document.createElement("tr");
    newTr.innerHTML = newString;
    let refTbody = document.getElementsByTagName("tbody");
    refTbody[0].appendChild(newTr);
     showPaseantes(tienda);
     muestraVisitas(tienda);
     muestraAtraccion(tienda);
     muestraGabinetes(tienda);
     muestraTickets(tienda);
     muestraCompra(tienda);
     muestraIngresos(tienda);
     muestraIngresosPorTicket(tienda);
     muestraArticulos(tienda);
     muestraItemTiket(tienda);
     muestraPromedioPermanencia(tienda);
 });

}


function showPaseantes (tienda){
    
    let brandData= "./../data/brandDateData.json";
    $.getJSON(brandData)
    .then(function(paseantes) {
    
        let i=0;
        let totalPeasants=0;
        paseantes.forEach(function(paseante){

           if(paseante.identifier === tienda.identifier){
                for (let key in paseante.peasants){
                    totalPeasants+=paseante.peasants[key];
                    
                }
               
                    let x=document.getElementsByClassName("paseante");
                    x[i].innerHTML=(totalPeasants);                  
            }
                i++;
        });
        
    });
  
}

function muestraVisitas(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalVisitas=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.visitors){
                    totalVisitas+=dataTienda.visitors[key];                    
                }                   
                let x=document.getElementsByClassName("visitas");
                     x[i].innerHTML=(totalVisitas);                    
               }
                i++;
            
        });
        
    });

}
function muestraGabinetes(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalGabinete=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.cabinet){
                    
                    totalGabinete+=dataTienda.cabinet[key];                    
                }                   
                let x=document.getElementsByClassName("gabinete");
                     x[i].innerHTML=(totalGabinete);                    
               }
                i++;
            
        });
        
    });
}
function muestraTickets(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalTickets=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.tickets){
                    totalTickets+=dataTienda.tickets[key];                    
                }                   
                let x=document.getElementsByClassName("tickets");
                     x[i].innerHTML=(totalTickets);                    
               }
                i++;
        });
        
    });
}

function muestraCompra(tienda){
    let brandData= "./../data/brandDateData.json";
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        let totalTickets=0;
        let totalVisitors =0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){ 
                    
               
                for (let key in dataTienda.visitors){
                    totalVisitors+=dataTienda.visitors[key];
                    
                }
                
               
                for (let key in dataTienda.tickets){
                    totalTickets+=dataTienda.tickets[key];
                    
                }
                
               // console.log(totalVisitor/totalPeasants)
               let x=document.getElementsByClassName("persuasion");
               x[i].innerHTML=((totalTickets/totalVisitors).toFixed(2)+"%");                         
               }
                i++;       
        });
    });
}
function muestraIngresos(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalIngresos=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.tickets){
                    totalIngresos+=dataTienda.revenue[key];                    
                }                   
                let x=document.getElementsByClassName("ingresos");
                     x[i].innerHTML=(totalIngresos);                    
               }
                i++;
            
        });
        
    });
}

function muestraIngresosPorTicket(tienda){
    let brandData= "./../data/brandDateData.json";
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        let totalTickets=0;
        let totalIngresos =0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){ 
                    
               
                for (let key in dataTienda.revenue){
                    totalIngresos+=dataTienda.revenue[key];
                    
                }
                
               
                for (let key in dataTienda.tickets){
                    totalTickets+=dataTienda.tickets[key];
                    
                }
                
               // console.log(totalVisitor/totalPeasants)
               let x=document.getElementsByClassName("ingresoTicket");
               x[i].innerHTML=((totalIngresos/totalTickets).toFixed(2)+"%");                         
               }
                i++;       
        });
    });
}

function muestraArticulos(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalItems=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.items){
                    totalItems+=dataTienda.items[key];                    
                }                   
                let x=document.getElementsByClassName("articulo");
                     x[i].innerHTML=(totalItems);                    
               }
                i++;
            
        });
        
    });
}
function muestraAtraccion(tienda){
    let brandData= "./../data/brandDateData.json";
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        let totalVisitor=0;
        let totalPeasants =0;
        dataTiendas.forEach(function(dataTienda){
            // console.log(dataTienda.visitors['2019-06-30'])
            // console.log(dataTienda.peasants['2019-06-30']);
           // console.log(dataTienda.visitors);
           
            if(dataTienda.identifier === tienda.identifier){ 
                    
               
                for (let key in dataTienda.visitors){
                    totalVisitor+=dataTienda.visitors[key];
                    
                }
                
               
                for (let key in dataTienda.peasants){
                    totalPeasants+=dataTienda.peasants[key];
                    
                }
                
               // console.log(totalVisitor/totalPeasants)
               let x=document.getElementsByClassName("atraccion");
               x[i].innerHTML=((totalVisitor/totalPeasants).toFixed(2)+"%");
               
                                     
               }
                i++;       
        });
    });

}
function muestraItemTiket(tienda){
    let brandData= "./../data/brandDateData.json";
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        let totalItems=0;
        let totalTickets =0;
        dataTiendas.forEach(function(dataTienda){       
            if(dataTienda.identifier === tienda.identifier){ 
                    
               
                for (let key in dataTienda.items){
                    totalItems+=dataTienda.items[key];
                    
                }
                
               
                for (let key in dataTienda.tickets){
                    totalTickets+=dataTienda.tickets[key];
                    
                }
                
               // console.log(totalVisitor/totalPeasants)
               let x=document.getElementsByClassName("articuloItem");
               x[i].innerHTML=(((totalItems/totalTickets)/100).toFixed(2));
               
                                     
               }
                i++;       
        });
    });

}

function muestraPromedioPermanencia(tienda){
    let brandData= "./../data/brandDateData.json";
    let totalPermanence=0;
    let totalPermanenceCount=0;
    $.getJSON(brandData)
    .then(function(dataTiendas) {
        let i=0;
        dataTiendas.forEach(function(dataTienda){
            if(dataTienda.identifier === tienda.identifier){
                for (let key in dataTienda.permanence){
                    totalPermanence+=dataTienda.permanence[key];                    
                } 
                for (let key in dataTienda.permanence){
                    totalPermanenceCount+=dataTienda.permanenceCount[key];                    
                }   
                 
                let x=document.getElementsByClassName("averagePermanence");
                x[i].innerHTML=(((totalPermanence*100)/totalPermanenceCount)/6000000);                    
               }
                i++;
            
        });
        
    });
}

$(document).ready(cargaPagina);

